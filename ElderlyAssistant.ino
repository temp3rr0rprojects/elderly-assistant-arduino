
#include <CurieBLE.h>
#include "CurieIMU.h"

// TODO: For future GPS lon/lat usage
int32_t latMsb = 0;
int32_t latLsb = 0;
int32_t lonMsb = 0;
int32_t lonLsb = 0;
bool verbose = false; // Disable printing console messages
BLEService batteryService("181F"); // BLE Battery Service
// BLE Battery Level Characteristic"
BLECharacteristic batteryLevelChar("3A19",  // standard 16-bit characteristic UUID
    BLERead | BLENotify, 6);     // remote clients will be able to get notifications if this characteristic changes
int oldBatteryLevel = 0;  // last battery level reading from analog input
long previousMillis = 0;  // last time the battery level was checked, in ms
boolean stepEventsEnabeled = true;   // whether you're polling or using events
long lastStepCount = 0;              // step count on previous polling check


void setup() {
  if (verbose)
    Serial.begin(115200);    // initialize serial communication
  pinMode(13, OUTPUT);   // initialize the LED on pin 13 to indicate when a central is connected
  
  BLE.begin();
  
  /* Initialise the IMU */
  CurieIMU.begin();
  CurieIMU.attachInterrupt(eventCallback);
  
  /* Enable Shock Detection */
  CurieIMU.setDetectionThreshold(CURIE_IMU_SHOCK, 3000); // Acceleration in g, ie 1.5g = 1500 mg
  CurieIMU.setDetectionDuration(CURIE_IMU_SHOCK, 100);   // Duration in ms, ie 50ms
  CurieIMU.interrupts(CURIE_IMU_SHOCK);   
  CurieIMU.setAccelerometerRange(4); // Increase Accelerometer range to allow detection of stronger taps (< 4g)  
  CurieIMU.setDetectionThreshold(CURIE_IMU_TAP, 1500); // Reduce threshold to allow detection of weaker taps (>= 750mg)  
  CurieIMU.interrupts(CURIE_IMU_TAP); // Enable Double-Tap detection  
  CurieIMU.setStepDetectionMode(CURIE_IMU_STEP_MODE_NORMAL); // turn on step detection mode:  
  CurieIMU.setStepCountEnabled(true); // enable step counting:
  CurieIMU.interrupts(CURIE_IMU_STEP);  // turn on step detection

  if (verbose)
    Serial.println("IMU initialisation complete, waiting for events...");

  /* Set a local name for the BLE device
     This name will appear in advertising packets
     and can be used by remote devices to identify this BLE device
     The name can be changed but maybe be truncated based on space left in advertisement packet
  */
  BLE.setLocalName("ElderlyAssistant");
  BLE.setAdvertisedService(batteryService);  // add the service UUID
  batteryService.addCharacteristic(batteryLevelChar); // add the battery level characteristic
  BLE.addService(batteryService);   // Add the BLE Battery service
  //batteryLevelChar.setValue(oldBatteryLevel);   // initial value for this characteristic

  const unsigned char defaultCharacteristicArray[6] = { 0, 0, 0, 0, 0, 0 };
  batteryLevelChar.setValue(defaultCharacteristicArray, 6);

  /* Start advertising BLE.  It will start continuously transmitting BLE
     advertising packets and will be visible to remote BLE central devices
     until it receives a new connection */

  // start advertising
  BLE.advertise();

  if (verbose)
    Serial.println("Bluetooth device active, waiting for connections...");
}

void loop() {

  if (!stepEventsEnabeled) {
    updateStepCount();
  }  
  
  BLEDevice central = BLE.central(); // listen for BLE peripherals to connect:

  // if a central is connected to peripheral:
  if (central) {
    if (verbose) {
      Serial.print("Connected to central: ");    
      Serial.println(central.address());
    }
    
    digitalWrite(13, HIGH); // turn on the LED to indicate the connection:

    // check the battery level every 200ms as long as the central is still connected:
    const int interval = 200; // 400 ms
    const int resetIntervalCounts = 60;
    int intervalCounts = 0;
    
    while (central.connected()) {
      long currentMillis = millis();
      
      if (currentMillis - previousMillis >= interval) { // if 200ms have passed, check the battery level:
        previousMillis = currentMillis;
        intervalCounts++;
        if (intervalCounts > resetIntervalCounts) {
          intervalCounts = 0;
          updateBatteryLevel(0);
        } else {        
          updateBatteryLevel(oldBatteryLevel);
        }
      }
    }
    
    digitalWrite(13, LOW); // when the central disconnects, turn off the LED:
    if (verbose) {
      Serial.print("Disconnected from central: ");
      Serial.println(central.address());
    }
  }
}

static void eventCallback(void) {
  if (CurieIMU.getInterruptStatus(CURIE_IMU_TAP)) {
    if (CurieIMU.tapDetected(X_AXIS, NEGATIVE))
      if (verbose)
        Serial.println("Tap detected on negative X-axis");
    if (CurieIMU.tapDetected(X_AXIS, POSITIVE))
      if (verbose)
        Serial.println("Tap detected on positive X-axis");
    if (CurieIMU.tapDetected(Y_AXIS, NEGATIVE))
      if (verbose)
        Serial.println("Tap detected on negative Y-axis");
    if (CurieIMU.tapDetected(Y_AXIS, POSITIVE))
      if (verbose)
        Serial.println("Tap detected on positive Y-axis");
    if (CurieIMU.tapDetected(Z_AXIS, NEGATIVE))
      if (verbose)
        Serial.println("Tap detected on negative Z-axis");
    if (CurieIMU.tapDetected(Z_AXIS, POSITIVE))
      if (verbose)
        Serial.println("Tap detected on positive Z-axis");
      
      updateBatteryLevel(1);
  }
  
  if (CurieIMU.getInterruptStatus(CURIE_IMU_SHOCK)) {
    if (CurieIMU.shockDetected(X_AXIS, POSITIVE))
      if (verbose)
        Serial.println("Negative shock detected on X-axis");
    if (CurieIMU.shockDetected(X_AXIS, NEGATIVE))
      if (verbose)
        Serial.println("Positive shock detected on X-axis");
    if (CurieIMU.shockDetected(Y_AXIS, POSITIVE))
      if (verbose)
        Serial.println("Negative shock detected on Y-axis");
    if (CurieIMU.shockDetected(Y_AXIS, NEGATIVE))
      if (verbose)
        Serial.println("Positive shock detected on Y-axis");
    if (CurieIMU.shockDetected(Z_AXIS, POSITIVE))
      if (verbose)
        Serial.println("Negative shock detected on Z-axis");
    if (CurieIMU.shockDetected(Z_AXIS, NEGATIVE))
      if (verbose)
        Serial.println("Positive shock detected on Z-axis");

      updateBatteryLevel(2);
  }

  if (CurieIMU.stepsDetected())
    updateStepCount();
}

void updateBatteryLevel(int shock) {
  /* Read the current voltage level on the A0 analog input pin.
     This is used here to simulate the charge level of a battery.
  */
  if (verbose) {
    Serial.print("Shock :");
    Serial.println(shock);
  }
  if (oldBatteryLevel != shock) {
    // TODO: fix lon/lat from signed -> unsigned @ nodejs
    const unsigned char characteristicArray[6] = { (char)shock, (char)lastStepCount, (char)latMsb, (char)latLsb, (char)lonMsb, (char)lonLsb };
    batteryLevelChar.setValue(characteristicArray, 6);
    oldBatteryLevel = shock;
  }
}

static void updateStepCount() {  
  int stepCount = CurieIMU.getStepCount(); // get the step count:

  if (stepCount != lastStepCount) { // if the step count has changed, print it
    if (verbose) {
      Serial.print("Step count: ");
      Serial.println(stepCount);
    }    
    lastStepCount = stepCount; // save the current count for comparison next check
  }
}
